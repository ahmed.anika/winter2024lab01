import java.util.Scanner;

public class Hangman {
	public static void main(String [] args){
		String word = "";
        boolean verifyWord = false;
// it will continue asking player 1 to enter a word until they type a  word with 4 different letters.
while (!verifyWord) {
//Player 1 will input a word 
		java.util.Scanner reader = new java.util.Scanner(System.in);
		System.out.println("Enter a 4 letter word:");
		word = reader.next();
// Word has to be composed of 4 letters
			if (word.length() != 4) {
			System.out.println("The word must have 4 different characters.");
            verifyWord = false;
            } else {
// All the letters have to be different
        char wordLetter1 = word.charAt(0);
        char wordLetter2 = word.charAt(1);
        char wordLetter3 = word.charAt(2);
        char wordLetter4 = word.charAt(3);

        if (wordLetter1 == wordLetter2 || wordLetter1 == wordLetter3 || wordLetter1 == wordLetter4 || wordLetter2 == wordLetter3 || wordLetter2 == wordLetter4 || wordLetter3 == wordLetter4) {
                    System.out.println("The word must have 4 different characters.");
					verifyWord = false;
// if validWord= true, the game will run
                } 
		else {
        verifyWord = true;


             }
           }
        }
        runGame(word);
    }

    
// Checks if the letter guessed is right, and if it's true, it will return the position or else it returns -1
	public static int isLetterInWord(String word, char c) {
		 c = toUpperCase(c);
        for (int i = 0; i < word.length(); i++) {
            if (toUpperCase(word.charAt(i)) == c) {
			
            return i;
            
       		 }
        
   		 }
		return -1;
	}
// Change characters to upper case 
	public static char toUpperCase(char c) {
		 if (c >= 'a' && c <= 'z'){
		 c -= 32;
		}
		 return c;

	}

	public static void printWork(String word, boolean letter0, boolean letter1, boolean letter2, boolean letter3) {
// Default output will be set to "Your result is _ _ _ _" 
// For each letter guessed correctly, it will appear on its rightful place i.e "Your result is _ o _ _" 

    	System.out.print("Your result is ");
    
   		 if (letter0) {
        System.out.print(word.charAt(0) + " ");
   			 } else {
     		 System.out.print("_ ");
   		 }
    
   		if (letter1) {
        System.out.print(word.charAt(1) + " ");
   			 } else {
       		 System.out.print("_ ");
    	}
    
   		 if (letter2) {
        System.out.print(word.charAt(2) + " ");
   			 } else {
      		  System.out.print("_ ");
    	}
    
   		 if (letter3) {
        System.out.println(word.charAt(3) + " ");
   		 } else {
        System.out.println("_ ");
   		}

   		
}

 	public static void runGame(String word) {
		int miss = 0;
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
        
		while (miss > -6 && ( !letter0 || !letter1|| !letter2 || !letter3)){
			java.util.Scanner reader = new java.util.Scanner(System.in);
			System.out.print("Enter a letter: ");
            char c = reader.next().charAt(0);
            int position = isLetterInWord(word, c);
// All boolean variables are set to false
            if (position != -1) {
// If the position was guessed right in the isLetterInWord method, the boolean will become true 
                if (position == 0) {
                    letter0 = true;
                } 
				else if (position == 1) {
                    letter1 = true;
                } 
				else if (position == 2) {
                    letter2 = true;
                } 
				else if (position == 3) {
                    letter3 = true;
                } 
            	} 
				else {
              		isLetterInWord(word, c);
					
// counts how many wrong guesses player 2 has. Once they have 6 misses or have completed the words, the game ends 
			miss--;

            }

            printWork(word, letter0, letter1, letter2, letter3);
        }
		if (miss == -6){
			 System.out.print("GAME OVER!");
		}  else  System.out.print("You have guessed the word correctly!");
	}	

}
 

